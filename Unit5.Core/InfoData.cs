﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
namespace Unit5.Core
{
    public class InfoData:INotifyPropertyChanged
    {
        public string Name
        {
            get { return this._name; }
            set
            {
                if (value != this._name)
                {
                    this._name = value;
                    this.PropertyChanged?.DynamicInvoke(this, new PropertyChangedEventArgs(nameof(this.Name)));
                }
            }
        }

        private string _name;


        public InfoData()
        {
            this.Name = "Steven";
            this.Email = "steven.chang@thinkpower.com.tw";
            this.Gander = Gander.Boy;
            this.Birthday = new DateTime(1999, 9, 9);
            this.ImagePath = "me.jpg";
        }

       public int Bonus { get; set; }
        public string Email { get; set; }
        public Gander Gander { get; set; }
        public DateTime Birthday { get; set; }
        public string ImagePath { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public  enum Gander
    {
        Boy=0,
        Girl=1
    }
}
