﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unit5.Core;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit5_DataBinding
{
    public class ImageNameConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string imageName = value.ToString();
            return ImageSource.FromResource($"Unit5_DataBinding.{imageName}");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Info : ContentPage
    {
        private InfoData data;
        public Info()
        {
            data = new InfoData();
            this.BindingContext = data;
            InitializeComponent();


            

         
        }

        private  void Button_Clicked(object sender, EventArgs e)
        {
            data.Name = "Orz";
            //this.imgMe.IsVisible = true;
           // this.DisplayAlert(string.Empty, data.Name, "OK");
        }
    }
}
